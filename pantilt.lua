-- Minimal VLC plug-in for IP camera pan/tilt control

local username = "admin"
local password = "plainbob"

function descriptor()
    return {
        title = "Pan/Tilt",
        version = "0.1",
        author = "Alan Sparrow",
        url = "https://freeflight.org.uk",
        shortdesc = "Pan/Tilt camera",
        capabilities = {"menu"},
    }
end

function activate()
    create_dialog()
end

function close()
    vlc.deactivate()
end

function deactivate()
end

function meta_changed()
end

function menu()
    return {"Show dialog"}
end

function trigger_menu(id)
    if id==1 then
        d:show()
    end
end

function create_dialog()
    local d = vlc.dialog(descriptor().title)

    d:add_button("Up", click_tilt_up, 2, 1, 1, 1)
    d:add_button("Left", click_pan_left, 1, 2, 1, 1)
    d:add_button("Right", click_pan_right, 3, 2, 1, 1)
    d:add_button("Down", click_tilt_down, 2, 3, 1, 1)
    d:add_button("Stop", click_stop, 2, 2, 1, 1)
end

function click_tilt_up()
    send_cmd(1)
end

function click_tilt_down()
    send_cmd(2)
end

function click_pan_right()
    send_cmd(4)
end

function click_pan_left()
    send_cmd(3)
end

function click_stop()
    send_cmd(0)
end

function send_cmd(cmd)
    local uri = vlc.input.item():uri()
    local host  = vlc.strings.url_parse(uri)["host"]

    local data = {
        "POST /form/setPTZCfg HTTP/1.1",
        "Host: "..host,
        "Authorization: Basic "..b64encode(username..":"..password),
        "",
        "command="..cmd.."&panSpeed=1&tiltSpeed=1"
    }
    local request = table.concat(data, "\r\n")

    local fd = vlc.net.connect_tcp(host, 80)
    vlc.net.send(fd, request)
    vlc.net.close(fd)
end

local b='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

function b64encode(data)
    return ((data:gsub('.', function(x) 
        local r,b='',x:byte()
        for i=8,1,-1 do r=r..(b%2^i-b%2^(i-1)>0 and '1' or '0') end
        return r;
    end)..'0000'):gsub('%d%d%d?%d?%d?%d?', function(x)
        if (#x < 6) then return '' end
        local c=0
        for i=1,6 do c=c+(x:sub(i,i)=='1' and 2^(6-i) or 0) end
        return b:sub(c+1,c+1)
    end)..({ '', '==', '=' })[#data%3+1])
end
